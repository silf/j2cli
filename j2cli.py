#!/usr/bin/env python

import codecs
import os
import sys
import jinja2


def render_template(file_name, ctx):
  with codecs.open(file_name, encoding="utf-8") as f:
    template = jinja2.Template(f.read())
  return template.render(**ctx)

if __name__ == "__main__":
  print(render_template(sys.argv[1], os.environ))