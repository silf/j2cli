from distutils.core import setup

setup(
  name='j2cli-simple',
  version='1.0',
  packages=[],
  url='',
  license='BSD',
  author='Jacek Bzdak',
  author_email='jbzdak@gmail.com',
  description='''Simpler version of j2cli tool. Contains 15 lines of code, and some tests.''',
  scripts=['j2cli.py'],
  classifiers=[
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: BSD License",
    "Operating System :: Unix",
    "Programming Language :: Python :: 3.5",
    "Programming Language :: Python :: 3.4",
    "Programming Language :: Python :: 2.7"
  ]
)
