import subprocess
import os

DIR = os.path.split(os.path.abspath(__file__))[0]

EXPECTED = """
A simple test for template.

Simple variable 10

Condition is true
"""


def test_cli():
    env = dict(**os.environ)
    env['VAR'] = '10'
    env['CONDITION'] = '1'
    output = subprocess.check_output(['j2cli.py', 'test.j2'], cwd=DIR, env=env)
    output = output.decode('utf-8')
    assert output.strip() == EXPECTED.strip()